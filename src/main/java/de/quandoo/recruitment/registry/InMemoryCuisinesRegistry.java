package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {


    private Map<String, List<Customer>> cusineCustomers = new HashMap<>();

    public InMemoryCuisinesRegistry() {
        cusineCustomers.put("italian", new LinkedList<>());
        cusineCustomers.put("french", new LinkedList<>());
        cusineCustomers.put("german", new LinkedList<>());

    }

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        String cuisineName = cuisine.getName();

        if (!cusineCustomers.containsKey(cuisineName)) {
            System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        }
        cusineCustomers.get(cuisineName).add(userId);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if(cuisine==null )return null;
        String cuisineName = cuisine.getName();
        if (!cusineCustomers.containsKey(cuisineName)) {
            return null;
        }
        return cusineCustomers.get(cuisineName);

    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        List<Cuisine> list = new ArrayList<>();

        for (String name : cusineCustomers.keySet()) {
            if (cusineCustomers.get(name).contains(customer)) list.add(new Cuisine(name));
        }


        if (list.size() == 0) return null;
        else return list;
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if(n>cusineCustomers.keySet().size()) throw new RuntimeException();
        return cusineCustomers.keySet().stream().sorted((o1, o2) -> Integer.compare(cusineCustomers.get(o2).size(), cusineCustomers.get(o1).size())).limit(n).map(Cuisine::new).collect(Collectors.toList());
    }
}
