package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);

    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }

    @Test
    public void testCuisineCustomer() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        assertEquals(customers.size(), 1);
        assertEquals(customers.get(0).getUuid(), "1");

        customers = cuisinesRegistry.cuisineCustomers(new Cuisine("german"));
        assertEquals(customers.size(), 2);
        assertEquals(customers.get(0).getUuid(), "2");

        customers = cuisinesRegistry.cuisineCustomers(new Cuisine("italian"));
        assertEquals(customers.size(), 0);
    }

    @Test
    public void testCustomerCuisine() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));

        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(new Customer("1"));
        assertEquals(cuisines.size(), 2);
        assertTrue(cuisines.containsAll(Arrays.asList(new Cuisine("french"), new Cuisine("german"))));

        cuisines = cuisinesRegistry.customerCuisines(new Customer("3"));
        assertEquals(cuisines.size(), 1);

    }

    @Test
    public void testTopCuisine() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));

        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);
        assertEquals(cuisines.size(), 2);
        assertTrue(cuisines.get(0).getName().equals("german"));
        assertTrue(cuisines.get(1).getName().equals("french"));

    }

}